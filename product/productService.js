import { generateRandomNumber, generateRandomNumberFloat } from "../util/utils.js";
let products = [];

export const getAllProducts = () => products;

export const generateNewProduct = (title, price, thumbnail) => {
  // hago lenght + 1 para que los id empiezen desde 1 y no desde 0
  const product = generateProduct(products.length + 1);
  if (title) product.title = title;
  if (price) product.price = price;
  if (thumbnail) product.thumbnail = thumbnail;

  products.push(product);
  return product;
};

export const getProductById = (id) => {
  const product = products.find((p) => p.id == id);
  return product;
};

export const deleteProductById = (id) => {
  const product = getProductById(id);
  products = products.filter((p) => p.id != id);
  return product;
};

export const updateProductById = (id, title, price, thumbnail) => {
  const index = products.findIndex((p) => p.id == id);
  if (index === -1) return;

  if (title) products[index].title = title;
  if (price) products[index].price = price;
  if (thumbnail) products[index].thumbnail = thumbnail;

  return products[index];
};

function generateProduct(id) {
  return {
    id,
    title: "Producto " + generateRandomNumber(1, 10),
    price: generateRandomNumberFloat(0.0, 9999.99),
    thumbnail: "Foto " + generateRandomNumber(1, 10),
  };
}
