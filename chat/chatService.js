import { File } from "../util/fileUtil.js"
const fileName = "chatHistory"

export const saveNewMessage = (msg) => {
    const chatFile = new File(fileName);
    chatFile.save(msg);
}

export const getAllMessagesFromFile = async () => {
    const chatFile = new File(fileName);
    const chatHistory = await chatFile.read();

    return JSON.parse(chatHistory);
}